#!/bin/bash
if [[ $# -eq 0 ]] ; then
    echo '😡 you must give a project name'
    exit 0
fi

directory_name=$1
go_version="1.17"

cp templates/simple-project/ ${directory_name} -r

# Create go.mod
cat > ${directory_name}/go.mod <<- EOM
module ${directory_name}

go ${go_version}
EOM

# Download wasm_exec.js
cp "$(go env GOROOT)/misc/wasm/wasm_exec.js" ./${directory_name}

