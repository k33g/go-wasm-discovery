# 🚧 work in progress
FROM gitpod/workspace-full
USER gitpod

RUN curl -sL https://raw.githubusercontent.com/moovweb/gvm/master/binscripts/gvm-installer| bash

RUN ["/bin/bash", "-c", ". /home/gitpod/.gvm/scripts/gvm && gvm install go1.17.5 -B"]
RUN ["/bin/bash", "-c", ". /home/gitpod/.gvm/scripts/gvm && gvm use go1.17.5"]

RUN wget https://github.com/tinygo-org/tinygo/releases/download/v0.21.0/tinygo_0.21.0_amd64.deb && \
    sudo dpkg -i tinygo_0.21.0_amd64.deb && \
    rm tinygo_0.21.0_amd64.deb

RUN brew install httpie && \
    brew install hey && \
    brew install bat && \
    brew install exa
